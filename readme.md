# Exercice docker


## Editer les fichiers de variables db.env et web.env avec vos paramètres

```
HOST_IP = adresse IP de la machine hôte docker
```

## Editer le fichier /etc/docker/daemon.json de la machine hôte docker

```bash
nano /etc/docker/daemon.json
```

```json
{
  "log-driver": "gelf",
  "log-opts": {
  "gelf-address": "udp://172.20.100.100:12201"
   }
}
```

## Redémarrer docker

```bash
service docker restart
```

## Choisir l'image adéquate pour gitlab

L'image sélectionné est celle compatible pour processeur ARM (MacBook M1/M2), si vous avez une machine hôte amd64 sélectionné l'image gitlab-ce officielle (décommenté la ligne adéquate)

## Lancer le docker compose

```bash
docker compose up -d
```
