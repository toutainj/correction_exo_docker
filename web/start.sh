#!/bin/bash
source /etc/apache2/envvars
if [ -z "$(ls -A /var/www/html/wordpress)" ]; then
   echo "Site web non installé"
   curl -s -u $HTTP_SRC_LOGIN:$HTTP_SRC_PWD $HTTP_SRC_WWW -o /var/www/src/website.zip > /dev/null
   curl -u $HTTP_SRC_LOGIN:$HTTP_SRC_PWD $HTTP_SRC_DB -o /var/www/src/db.sql 
   unzip /var/www/src/*.zip -d /var/www/html/
   sed -i 's/10.76.1.72/'$HOST_IP'/g' /var/www/src/*.sql
   sed -i 's/10.176.128.142/'$HOST_IP'/g' /var/www/src/*.sql
   rm /var/www/src/website.zip
   mysql -h db -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE < /var/www/src/db.sql
   #rm /var/www/src/*
   sed -i 's/'DB_HOST', 'wordpress'/'DB_HOST', '$MYSQL_HOST'/g' /var/www/html/*/wp-config.php
   sed -i 's/'DB_NAME', 'wordpress'/'DB_NAME', '$MYSQL_DATABASE'/g' /var/www/html/*/wp-config.php
   sed -i 's/'DB_USER', 'wordpress'/'DB_USER', '$MYSQL_USER'/g' /var/www/html/*/wp-config.php
   sed -i 's/'DB_PASSWORD', 'wordpress'/'DB_PASSWORD', '$MYSQL_PASSWORD'/g' /var/www/html/*/wp-config.php
   rm /var/www/html/*/.htaccess

else
   echo "Site web déjà installé"
fi
exec apache2 -D FOREGROUND
